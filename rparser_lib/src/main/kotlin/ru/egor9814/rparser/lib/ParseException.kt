package ru.egor9814.rparser.lib

class ParseException(val position: Position, message: String) : RuntimeException("$position $message")
