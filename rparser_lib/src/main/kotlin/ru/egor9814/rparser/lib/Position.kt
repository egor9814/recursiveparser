package ru.egor9814.rparser.lib

data class Position(val row: Int, val col: Int) {
    override fun toString(): String {
        return "[$row:$col]"
    }
}