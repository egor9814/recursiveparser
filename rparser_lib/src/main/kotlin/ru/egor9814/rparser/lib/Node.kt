package ru.egor9814.rparser.lib

import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.nio.charset.Charset
import java.util.*

abstract class Node(val name: String) : INode, Iterable<INode> {

    override val isTerminal = false


    private val children = mutableListOf<INode>()

    fun addChild(node: INode) = apply {
        children.add(node)
    }

    fun getChildren() = children.toList()

    override fun iterator(): Iterator<INode> {
        return children.iterator()
    }

    override fun spliterator(): Spliterator<INode> {
        return children.spliterator()
    }

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }

    override fun toString(): String {
        return joinToString(separator = " .. ", prefix = "{ ", postfix = " }") {
            it.toString()
        }
    }
}


class TerminalNode(val value: Char) : INode {
    override val isTerminal = true

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }

    override fun toString(): String {
        return "'$value'"
    }
}
