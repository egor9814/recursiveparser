package ru.egor9814.rparser.lib

interface IInputCharStream {

    fun open(): Boolean

    fun close()

    fun hasNext(): Boolean

    fun next(): Char

}