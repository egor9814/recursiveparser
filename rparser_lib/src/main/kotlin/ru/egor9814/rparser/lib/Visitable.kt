package ru.egor9814.rparser.lib

interface Visitable {

    fun <A> apply(visitor: Visitor<A>, arg: A)

}
