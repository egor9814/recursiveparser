package ru.egor9814.rparser.lib

interface Visitor<A> {

    fun visit(node: INode, arg: A)

}
