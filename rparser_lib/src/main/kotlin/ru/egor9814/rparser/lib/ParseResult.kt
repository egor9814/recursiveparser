package ru.egor9814.rparser.lib

sealed class ParseResult

class SuccessParseResult<N : INode>(val node: N) : ParseResult()

class FailedParseResult(val message: String) : ParseResult()
