package ru.egor9814.rparser.lib

interface INode : Visitable {
    val isTerminal: Boolean
}
