package ru.egor9814.rparser.lib

import kotlin.reflect.KClass

abstract class ParserBase(
    val source: SourceCode
) {

    init {
        source.reset()
    }

    fun reset() {
        source.reset()
    }

    protected fun ws(node: INode? = null): Boolean {
        while (source[0] in " \r\n\t") {
            source.consume(source.getBuffer(1))
        }
        return true
    }

    protected fun check(char: Char, node: INode? = null): Boolean {
        if (source[0] == char) {
            if (node is Node)
                node.addChild(TerminalNode(char))
            return source.consume(source.getBuffer(1))
        }
        return false
    }

    /*protected fun checkNot(char: Char): Boolean {
        if (source[0] != char) {
            return source.consume(source.getBuffer(1))
        }
        return false
    }

    protected fun skip(count: Int): Boolean {
        return source.consume(source.getBuffer(count))
    }*/

    protected fun anyChar(node: INode? = null): Boolean {
        if (node is Node)
            node.addChild(TerminalNode(source[0]))
        return source.consume(source.getBuffer(1))
    }


    protected abstract fun main(result: Node): Boolean


    class ResultNode : Node("Result")

    fun <N : INode> parse(): ParseResult {
        val result = ResultNode()
        var errorMessage = ""
        val success = try {
            main(result) && source.available() == 0
        } catch (err: Throwable) {
            errorMessage = err.message ?: "<unknown error>"
            false
        }
        return if (success)
            result.firstOrNull()?.let {
                checkedCast<N>(it)?.let { n -> SuccessParseResult(n) }
            } ?: FailedParseResult("Invalid parse node type")
        else
            FailedParseResult(errorMessage)
    }

    private fun <T> checkedCast(value: Any): T? {
        return try {
            value as T
        } catch (err: TypeCastException) {
            null
        } catch (err: ClassCastException) {
            null
        }
    }


    protected fun error(message: String): Boolean {
        throw ParseException(source.codePosition, message)
    }


    protected fun checkSource(state: Int?): Boolean {
        if (state != null && source.position != state) {
            error("Invalid symbol sequence")
        }
        return false
    }

    protected fun save() = source.position

}