/* Auto-Generated Recursive Parser by egor9814 */

package emera

import ru.egor9814.rparser.lib.*

class EmeraParser(sourceCode: SourceCode) : ParserBase(sourceCode) {
    class IdNode : Node("Id")
    private fun rid(parentNode: Node?): Boolean {
        ws()
        val rn = IdNode()
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0061', rn)
            if (r) break else r = check('\u0062', rn)
            if (r) break else r = check('\u0063', rn)
            if (r) break else r = check('\u0064', rn)
            if (r) break else r = check('\u0065', rn)
            if (r) break else r = check('\u0066', rn)
            if (r) break else r = check('\u0067', rn)
            if (r) break else r = check('\u0068', rn)
            if (r) break else r = check('\u0069', rn)
            if (r) break else r = check('\u006A', rn)
            if (r) break else r = check('\u006B', rn)
            if (r) break else r = check('\u006C', rn)
            if (r) break else r = check('\u006D', rn)
            if (r) break else r = check('\u006E', rn)
            if (r) break else r = check('\u006F', rn)
            if (r) break else r = check('\u0070', rn)
            if (r) break else r = check('\u0071', rn)
            if (r) break else r = check('\u0072', rn)
            if (r) break else r = check('\u0073', rn)
            if (r) break else r = check('\u0074', rn)
            if (r) break else r = check('\u0075', rn)
            if (r) break else r = check('\u0076', rn)
            if (r) break else r = check('\u0077', rn)
            if (r) break else r = check('\u0078', rn)
            if (r) break else r = check('\u0079', rn)
            if (r) break else r = check('\u007A', rn)
            if (r) break else r = check('\u0041', rn)
            if (r) break else r = check('\u0042', rn)
            if (r) break else r = check('\u0043', rn)
            if (r) break else r = check('\u0044', rn)
            if (r) break else r = check('\u0045', rn)
            if (r) break else r = check('\u0046', rn)
            if (r) break else r = check('\u0047', rn)
            if (r) break else r = check('\u0048', rn)
            if (r) break else r = check('\u0049', rn)
            if (r) break else r = check('\u004A', rn)
            if (r) break else r = check('\u004B', rn)
            if (r) break else r = check('\u004C', rn)
            if (r) break else r = check('\u004D', rn)
            if (r) break else r = check('\u004E', rn)
            if (r) break else r = check('\u004F', rn)
            if (r) break else r = check('\u0050', rn)
            if (r) break else r = check('\u0051', rn)
            if (r) break else r = check('\u0052', rn)
            if (r) break else r = check('\u0053', rn)
            if (r) break else r = check('\u0054', rn)
            if (r) break else r = check('\u0055', rn)
            if (r) break else r = check('\u0056', rn)
            if (r) break else r = check('\u0057', rn)
            if (r) break else r = check('\u0058', rn)
            if (r) break else r = check('\u0059', rn)
            if (r) break else r = check('\u005A', rn)
            if (r) break else r = check('\u005F', rn)
            if (r) break else r = check('\u0024', rn)
            break
        } else return checkSource(state)
        if (r) do {
            state = save()
            r = rrid1ZoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    class NumberNode : Node("Number")
    private fun rnumber(parentNode: Node?): Boolean {
        ws()
        val rn = NumberNode()
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = rrnumber0(rn)
            if (r) break else r = rrnumber1(rn)
            break
        } else return checkSource(state)
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    class LiteralNode : Node("Literal")
    private fun rliteral(parentNode: Node?): Boolean {
        ws()
        val rn = LiteralNode()
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = rnumber(rn)
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    class SimpleExprNode : Node("SimpleExpr")
    private fun rsimpleExpr(parentNode: Node?): Boolean {
        ws()
        val rn = SimpleExprNode()
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = rrsimpleExpr0(rn)
            if (r) break else r = rrsimpleExpr1(rn)
            if (r) break else r = rrsimpleExpr2(rn)
            break
        } else return checkSource(state)
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    class MultExprNode : Node("MultExpr")
    private fun rmultExpr(parentNode: Node?): Boolean {
        ws()
        val rn = MultExprNode()
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = rsimpleExpr(rn)
        if (r) do {
            state = save()
            r = rrmultExpr1ZoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    class AddExprNode : Node("AddExpr")
    private fun raddExpr(parentNode: Node?): Boolean {
        ws()
        val rn = AddExprNode()
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = rmultExpr(rn)
        if (r) do {
            state = save()
            r = rraddExpr1ZoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    class ExprNode : Node("Expr")
    private fun rexpr(parentNode: Node?): Boolean {
        ws()
        val rn = ExprNode()
        var r = true
        var state: Int? = null
        ws()
        if (!r) return checkSource(state) else r = raddExpr(rn)
        ws()
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    class MainNode : Node("Main")
    private fun rmain(parentNode: Node?): Boolean {
        ws()
        val rn = MainNode()
        var r = true
        var state: Int? = null
        if (r) do {
            state = save()
            r = rrmainZoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        if (!r) checkSource(state)
        return r
    }
    private fun rrid1ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = rrrid1ZoM0(rn)
            if (r) break else r = rrrid1ZoM1(rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrnumber0(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = check('\u0030', rn)
        state = save()
        if (r) while (true) {
            r = rrrnumber010(rn)
            if (r) break else r = rrrnumber011(rn)
            if (r) break else r = rrrnumber012(rn)
            if (r) break else r = rrrnumber013(rn)
            if (r) break else r = rrrnumber014(rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrnumber1(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            if (r) break else r = check('\u0038', rn)
            if (r) break else r = check('\u0039', rn)
            break
        } else return checkSource(state)
        if (r) do {
            state = save()
            r = rrrnumber11ZoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (r) {
            state = save()
            r = rrrnumber12ZoO(rn)
        } else return checkSource(state)
        r = true
        if (!r) checkSource(state)
        return r
    }
    private fun rrsimpleExpr0(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = rliteral(rn)
        if (!r) checkSource(state)
        return r
    }
    private fun rrsimpleExpr1(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = rid(rn)
        if (!r) checkSource(state)
        return r
    }
    private fun rrsimpleExpr2(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = check('\u0028', rn)
        if (!r) return checkSource(state) else r = rexpr(rn)
        if (!r) return checkSource(state) else r = check('\u0029', rn)
        if (!r) checkSource(state)
        return r
    }
    private fun rrmultExpr1ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        ws()
        state = save()
        if (r) while (true) {
            r = check('\u002A', rn)
            if (r) break else r = check('\u002F', rn)
            if (r) break else r = check('\u005C', rn)
            break
        } else return checkSource(state)
        ws()
        if (!r) return checkSource(state) else r = rsimpleExpr(rn)
        if (!r) checkSource(state)
        return r
    }
    private fun rraddExpr1ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        ws()
        state = save()
        if (r) while (true) {
            r = check('\u002B', rn)
            if (r) break else r = check('\u002D', rn)
            break
        } else return checkSource(state)
        ws()
        if (!r) return checkSource(state) else r = rmultExpr(rn)
        if (!r) checkSource(state)
        return r
    }
    private fun rrmainZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = rexpr(rn)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrid1ZoM0(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0061', rn)
            if (r) break else r = check('\u0062', rn)
            if (r) break else r = check('\u0063', rn)
            if (r) break else r = check('\u0064', rn)
            if (r) break else r = check('\u0065', rn)
            if (r) break else r = check('\u0066', rn)
            if (r) break else r = check('\u0067', rn)
            if (r) break else r = check('\u0068', rn)
            if (r) break else r = check('\u0069', rn)
            if (r) break else r = check('\u006A', rn)
            if (r) break else r = check('\u006B', rn)
            if (r) break else r = check('\u006C', rn)
            if (r) break else r = check('\u006D', rn)
            if (r) break else r = check('\u006E', rn)
            if (r) break else r = check('\u006F', rn)
            if (r) break else r = check('\u0070', rn)
            if (r) break else r = check('\u0071', rn)
            if (r) break else r = check('\u0072', rn)
            if (r) break else r = check('\u0073', rn)
            if (r) break else r = check('\u0074', rn)
            if (r) break else r = check('\u0075', rn)
            if (r) break else r = check('\u0076', rn)
            if (r) break else r = check('\u0077', rn)
            if (r) break else r = check('\u0078', rn)
            if (r) break else r = check('\u0079', rn)
            if (r) break else r = check('\u007A', rn)
            if (r) break else r = check('\u0041', rn)
            if (r) break else r = check('\u0042', rn)
            if (r) break else r = check('\u0043', rn)
            if (r) break else r = check('\u0044', rn)
            if (r) break else r = check('\u0045', rn)
            if (r) break else r = check('\u0046', rn)
            if (r) break else r = check('\u0047', rn)
            if (r) break else r = check('\u0048', rn)
            if (r) break else r = check('\u0049', rn)
            if (r) break else r = check('\u004A', rn)
            if (r) break else r = check('\u004B', rn)
            if (r) break else r = check('\u004C', rn)
            if (r) break else r = check('\u004D', rn)
            if (r) break else r = check('\u004E', rn)
            if (r) break else r = check('\u004F', rn)
            if (r) break else r = check('\u0050', rn)
            if (r) break else r = check('\u0051', rn)
            if (r) break else r = check('\u0052', rn)
            if (r) break else r = check('\u0053', rn)
            if (r) break else r = check('\u0054', rn)
            if (r) break else r = check('\u0055', rn)
            if (r) break else r = check('\u0056', rn)
            if (r) break else r = check('\u0057', rn)
            if (r) break else r = check('\u0058', rn)
            if (r) break else r = check('\u0059', rn)
            if (r) break else r = check('\u005A', rn)
            if (r) break else r = check('\u005F', rn)
            if (r) break else r = check('\u0024', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrid1ZoM1(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            if (r) break else r = check('\u0038', rn)
            if (r) break else r = check('\u0039', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrnumber010(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = check('\u0078', rn)
        if (!r) return checkSource(state)
        state = save()
        r = rrrrnumber0101OoM(rn)
        if (r) do {
            state = save()
            r = rrrrnumber0101OoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (!r) checkSource(state)
        return r
    }
    private fun rrrnumber011(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = check('\u006F', rn)
        if (!r) return checkSource(state)
        state = save()
        r = rrrrnumber0111OoM(rn)
        if (r) do {
            state = save()
            r = rrrrnumber0111OoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (!r) checkSource(state)
        return r
    }
    private fun rrrnumber012(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = check('\u0062', rn)
        if (!r) return checkSource(state)
        state = save()
        r = rrrrnumber0121OoM(rn)
        if (r) do {
            state = save()
            r = rrrrnumber0121OoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (!r) checkSource(state)
        return r
    }
    private fun rrrnumber013(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (r) do {
            state = save()
            r = rrrrnumber013ZoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (!r) checkSource(state)
        return r
    }
    private fun rrrnumber014(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = check('\u002E', rn)
        if (!r) return checkSource(state)
        state = save()
        r = rrrrnumber0141OoM(rn)
        if (r) do {
            state = save()
            r = rrrrnumber0141OoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (!r) checkSource(state)
        return r
    }
    private fun rrrnumber11ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            if (r) break else r = check('\u0038', rn)
            if (r) break else r = check('\u0039', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrnumber12ZoO(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        if (!r) return checkSource(state) else r = check('\u002E', rn)
        if (!r) return checkSource(state)
        state = save()
        r = rrrrnumber12ZoO1OoM(rn)
        if (r) do {
            state = save()
            r = rrrrnumber12ZoO1OoM(rn)
        } while (r) else return checkSource(state)
        r = true
        if (!r) checkSource(state)
        return r
    }
    private fun rrrrnumber0101OoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            if (r) break else r = check('\u0038', rn)
            if (r) break else r = check('\u0039', rn)
            if (r) break else r = check('\u0061', rn)
            if (r) break else r = check('\u0062', rn)
            if (r) break else r = check('\u0063', rn)
            if (r) break else r = check('\u0064', rn)
            if (r) break else r = check('\u0065', rn)
            if (r) break else r = check('\u0066', rn)
            if (r) break else r = check('\u0041', rn)
            if (r) break else r = check('\u0042', rn)
            if (r) break else r = check('\u0043', rn)
            if (r) break else r = check('\u0044', rn)
            if (r) break else r = check('\u0045', rn)
            if (r) break else r = check('\u0046', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrrnumber0111OoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrrnumber0121OoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrrnumber013ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            if (r) break else r = check('\u0038', rn)
            if (r) break else r = check('\u0039', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrrnumber0141OoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            if (r) break else r = check('\u0038', rn)
            if (r) break else r = check('\u0039', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    private fun rrrrnumber12ZoO1OoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        var state: Int? = null
        state = save()
        if (r) while (true) {
            r = check('\u0030', rn)
            if (r) break else r = check('\u0031', rn)
            if (r) break else r = check('\u0032', rn)
            if (r) break else r = check('\u0033', rn)
            if (r) break else r = check('\u0034', rn)
            if (r) break else r = check('\u0035', rn)
            if (r) break else r = check('\u0036', rn)
            if (r) break else r = check('\u0037', rn)
            if (r) break else r = check('\u0038', rn)
            if (r) break else r = check('\u0039', rn)
            break
        } else return checkSource(state)
        if (!r) checkSource(state)
        return r
    }
    override fun main(result: Node): Boolean {
        return rmain(result)
    }
}
