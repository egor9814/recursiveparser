package emera.ast

interface Visitor<A> {

    fun visit(e: NumberExpr, arg: A)
    fun visit(e: VarExpr, arg: A)
    fun visit(e: AddExpr, arg: A)
    fun visit(e: MultExpr, arg: A)

    fun visit(c: CompilationUnit, arg: A)

}