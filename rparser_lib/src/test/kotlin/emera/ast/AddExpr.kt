package emera.ast

class AddExpr(val left: Expression, val right: Expression, val o: Operator) : Expression() {

    enum class Operator(val representation: String) {
        ADD("+"),
        SUB("-");

        companion object {
            fun find(representation: String) = values().find { it.representation == representation }
        }
    }

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }

    override fun toString(): String {
        return "($left ${o.representation} $right)"
    }

}
