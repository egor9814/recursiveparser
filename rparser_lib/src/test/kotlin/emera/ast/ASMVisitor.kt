package emera.ast

import java.io.PrintStream

class ASMVisitor(val out: PrintStream) : Visitor<Int> {

    companion object {
        fun asm(unit: CompilationUnit, out: PrintStream) {
            unit.apply(ASMVisitor(out), 0)
        }
    }

    override fun visit(e: NumberExpr, arg: Int) {
        out.println("load_const ${e.value}")
    }

    override fun visit(e: VarExpr, arg: Int) {
        out.println("load ${e.name}")
    }

    override fun visit(e: AddExpr, arg: Int) {
        e.right.apply(this, arg)
        e.left.apply(this, arg)
        out.println(e.o.name.toLowerCase())
    }

    override fun visit(e: MultExpr, arg: Int) {
        e.right.apply(this, arg)
        e.left.apply(this, arg)
        out.println(e.o.name.toLowerCase())
    }

    override fun visit(c: CompilationUnit, arg: Int) {
        c.expressions.forEach {
            it.apply(this, arg)
        }
    }

}