package emera.ast

class VarExpr(val name: String) : Expression() {

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }

    override fun toString(): String {
        return name
    }

}
