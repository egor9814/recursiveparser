package emera.ast

interface Visitable {

    fun <A> apply(visitor: Visitor<A>, arg: A)

}