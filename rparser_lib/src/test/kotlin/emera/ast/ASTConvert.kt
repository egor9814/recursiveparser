package emera.ast

import emera.EmeraParser.*
import ru.egor9814.rparser.lib.TerminalNode

private fun ru.egor9814.rparser.lib.Node.terminalsToString() = joinToString(separator = "") {
    require(it is TerminalNode)
    it.value.toString()
}

fun IdNode.toExpr(): Expression {
    return VarExpr(
            terminalsToString()
    )
}

fun NumberNode.toExpr(): Expression {
    return NumberExpr(
            terminalsToString()
    )
}

fun LiteralNode.toExpr(): Expression {
    return first().let {
        require(it is NumberNode)
        it.toExpr()
    }
}

fun SimpleExprNode.toExpr(): Expression {
    return when (val c = first()) {
        is LiteralNode -> c.toExpr()
        is IdNode -> c.toExpr()
        is TerminalNode -> {
            require(c.value == '(' && last().let { it is TerminalNode && it.value == ')' })
            val e = getChildren()[1]
            require(e is ExprNode)
            e.toExpr()
        }
        else -> throw RuntimeException()
    }
}

fun MultExprNode.toExpr(): Expression {
    var e = first().let {
        require(it is SimpleExprNode)
        it.toExpr()
    }
    var o = '\u0000'
    drop(1).forEach {
        if (o == '\u0000') {
            require(it is TerminalNode)
            o = it.value
        } else {
            require(it is SimpleExprNode)
            val right = it.toExpr()
            e = MultExpr(e, right, MultExpr.Operator.find(o.toString())!!)
            o = '\u0000'
        }
    }
    return e
}

fun AddExprNode.toExpr(): Expression {
    var e = first().let {
        require(it is MultExprNode)
        it.toExpr()
    }
    var o = '\u0000'
    drop(1).forEach {
        if (o == '\u0000') {
            require(it is TerminalNode)
            o = it.value
        } else {
            require(it is MultExprNode)
            val right = it.toExpr()
            e = AddExpr(e, right, AddExpr.Operator.find(o.toString())!!)
            o = '\u0000'
        }
    }
    return e
}

fun ExprNode.toExpr(): Expression {
    return first().let {
        require(it is AddExprNode)
        it.toExpr()
    }
}

fun MainNode.toCompilationUnit(): CompilationUnit {
    val result = mutableListOf<Expression>()
    forEach {
        require(it is ExprNode)
        result.add(it.toExpr())
    }
    return CompilationUnit(result.toList())
}
