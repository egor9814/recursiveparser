package emera.ast

class NumberExpr(val value: String) : Expression() {

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }

    override fun toString(): String {
        return value
    }

}
