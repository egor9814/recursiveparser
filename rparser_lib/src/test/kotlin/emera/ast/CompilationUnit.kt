package emera.ast

class CompilationUnit(val expressions: List<Expression>) : Node() {
    override fun toString(): String {
        return expressions.joinToString(separator = "\n")
    }

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }
}
