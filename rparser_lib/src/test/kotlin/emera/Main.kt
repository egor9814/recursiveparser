package emera

import emera.ast.ASMVisitor
import emera.ast.toCompilationUnit
import ru.egor9814.rparser.lib.FailedParseResult
import ru.egor9814.rparser.lib.PrettyPrintNodeTree
import ru.egor9814.rparser.lib.SourceCode
import ru.egor9814.rparser.lib.SuccessParseResult

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val source = SourceCode(
            //"`cap at'e\tkek`"
                "abc + 0x123Fe - 1.0\n" +
                        "get * (smart - tv)"
        )
        val parser = EmeraParser(source)
        val result = parser.parse<EmeraParser.MainNode>()
        if (result is FailedParseResult) {
            println("invalid code: ${result.message}")
        } else {
            require(result is SuccessParseResult<*>)

            println("Parse tree:")
            println(result.node.toString())

            println("\nPretty print tree:")
            PrettyPrintNodeTree.print(result.node, System.out)

            val unit = result.node.let {
                it as EmeraParser.MainNode
            }.toCompilationUnit()
            println("\nUnit:\n$unit")

            println("\nASM:")
            ASMVisitor.asm(unit, System.out)
        }
    }
}
