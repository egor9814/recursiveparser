package emera

import ru.egor9814.rparser.builder.ParserBuilder
import ru.egor9814.rparser.builder.SourceWriter
import ru.egor9814.rparser.design.LangConfiguration
import ru.egor9814.rparser.design.RulesConfiguration
import ru.egor9814.rparser.design.Symbol
import java.io.File

class EmeraRules : RulesConfiguration() {

    private object WS : RulesConfiguration() {
        operator fun invoke(rule: () -> Symbol) = ws .. rule() .. ws
    }

    init {
        // '0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'
        val digit by fragment {
            T["0-9"]
        }

        // 'a'..'z' | 'A'..'Z' | '_' | '$'
        val idStart by fragment {
            T["a-zA-Z_$"]
        }

        // idStart | digit
        val idPart by fragment {
            idStart or digit
        }

        // idStart idPart{0,1,2...}
        rule("id") {
            (idStart .. N[idPart]{ zero..n })/* or (T['`'] .. N[!(T['\n'] or T['\r'] or T['`'])]{one..n} .. T['`'])*/
        }


        // '0x' ('0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'|'a'|'b'|'c'|'d'|'e'|'f'|'A'|'B'|'C'|'D'|'E'|'F'){0,1,2...}
        val hexInt by fragment {
            T['x'] .. N[T["0-9a-fA-F"]]{1..n}
        }
        // '0o' ('0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'){0,1,2...}
        val octInt by fragment {
            T['o'] .. N[T["0-7"]]{1..n}
        }
        // '0b' ('0'|'1'){0,1,2...}
        val binInt by fragment {
            T['b'] .. N[T["01"]]{1..n}
        }
        // ... '.' ('0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'){0,1,2...}
        val float by fragment {
            T['.'] .. N[digit]{1..n}
        }
        // hexInt | octInt | binInt | int | float
        rule("number") {
            (T['0'] .. (hexInt or octInt or binInt or N[digit]{0..n} or float)) or (T["1-9"] .. N[digit]{0..n} .. N[float]{0..one})
        }


        // number
        rule("literal") {
            R{"number"}
        }


        // literal | id | '(' expr ')'
        rule("simpleExpr") {
            R{"literal"} or R{"id"} or ( T['('] .. R{"expr"} .. T[')'] )
        }


        // simpleExpr ( ('*' | '/' | '\') simpleExpr){0,1,2...}
        rule("multExpr") {
            R{"simpleExpr"} .. N[ WS{ T['*'] or T['/'] or T['\\'] } .. R{"simpleExpr"} ]{0..n}
        }


        // multExpr ( ('+' | '-') multExpr){0,1,2...}
        rule("addExpr") {
            R{"multExpr"} .. N[ WS{ T['+'] or T['-'] } .. R{"multExpr"} ]{0..n}
        }


        // addExpr
        rule("expr") {
            WS{ R{"addExpr"} }
        }


        // epxr
        rule("main") {
            N[ R{"expr"} ]{0..n}
        }
    }

}


class EmeraConf : LangConfiguration() {
    init {
        packageName = "emera"
        langName = "Emera"
        author = arrayOf("egor9814")

        setRules(EmeraRules()) {
            "main"
        }
    }
}


object EmeraLangGenerator {
    @JvmStatic
    fun main(args: Array<String>) {
        val wd = File("").absoluteFile
        val out = File(wd, "rparser_lib/src/test/kotlin")
        ParserBuilder(EmeraConf()).build(out)
    }
}
