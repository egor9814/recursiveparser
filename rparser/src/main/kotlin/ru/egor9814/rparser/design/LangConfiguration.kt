package ru.egor9814.rparser.design

abstract class LangConfiguration {

    var packageName = ""
    var langName = ""
    var author = emptyArray<String>()

    /*protected abstract fun createRules(): RC

    val rules by lazy { createRules() }*/

    private lateinit var mRules: RulesConfiguration
    private lateinit var mMain: String

    protected fun setRules(rules: RulesConfiguration, mainRule: () -> String) {
        mRules = rules
        mMain = mainRule()
    }

    val rules: RulesConfiguration
        get() = mRules

    val mainRule: String
        get() = mMain

}