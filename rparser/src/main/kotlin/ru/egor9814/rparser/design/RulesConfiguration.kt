package ru.egor9814.rparser.design

import ru.egor9814.rparser.builder.SourceWriter
import kotlin.reflect.KProperty

abstract class RulesConfiguration {

    companion object {
        private fun wrapEscape(char: Char) = char.let {
            var code = it.toInt().toString(16).toUpperCase()
            while (code.length < 4)
                code = "0$code"
            "\\u$code"
        }
    }

    private class Terminal(val char: Char) : Symbol {
        override val isTerminal = true

        override fun toString(): String {
            return "'${wrapEscape(char)}'"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("check('${wrapEscape(char)}', rn)")
            }
        }
    }

    private abstract class NonTerminal : Symbol {
        override val isTerminal = false
    }


    object T {
        operator fun get(value: Char): Symbol {
            return Terminal(value)
        }

        operator fun get(value: String): Symbol {
            require(value.isNotEmpty()) {
                "String must contain one char or more"
            }
            val chars = mutableSetOf<Char>()
            var i = 0
            while (i < value.length) {
                if (i > 0 && value[i] == '-' && i + 1 < value.length) {
                    if (value[i + 1] == '-') {
                        chars.add('-')
                        i++
                    } else {
                        for (c in (value[i-1] + 1)..(value[++i])) {
                            chars.add(c)
                        }
                    }
                } else {
                    chars.add(value[i])
                }
                i++
            }
            return if (chars.size == 1) {
                Terminal(chars.first())
            } else {
                Alternatives().apply {
                    symbols.addAll(chars.map { Terminal(it) })
                }
            }
        }

        operator fun invoke(value: String): Symbol {
            require(value.isNotEmpty()) {
                "String must contain one char or more"
            }
            return Sequence().apply {
                symbols.addAll(value.map { Terminal(it) })
            }
        }
    }


    private class Alternatives : NonTerminal() {
        val symbols = mutableListOf<Symbol>()

        override fun toString(): String {
            return symbols.joinToString(prefix = "<", postfix = ">", separator = " | ") {
                "($it)"
            }
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("state = save()")
                println("if (r) while (true) {")
                indent {
                    symbols.withIndex().forEach { (i, s) ->
                        if (i == 0) {
                            print("r = ")
                        } else {
                            print("if (r) break else r = ")
                        }
                        if (s.isTerminal) {
                            s.generateCode(0, "$currentRuleName$i", out)
                        } else {
                            println("r$currentRuleName$i(rn)", 0)
                            doLater {
                                func("r$currentRuleName$i", body = s::generateCode)
                            }
                        }
                    }
                    println("break")
                }
                println("} else return checkSource(state)")
            }
        }
    }

    infix fun Symbol.or(other: Symbol): Symbol {
        return if (this is Alternatives) {
            this.apply {
                symbols.add(other)
            }
        } else {
            Alternatives().apply {
                symbols.add(this@or)
                symbols.add(other)
            }
        }
    }


    private class Sequence : NonTerminal() {
        val symbols = mutableListOf<Symbol>()

        override fun toString(): String {
            return symbols.joinToString(prefix = "<", postfix = ">", separator = " ") {
                "($it)"
            }
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.apply {
                symbols.withIndex().forEach { (i, s) ->
                    if (s.isTerminal) {
                        out.withIndent(indentLevel) {
                            //print("if (!r) return error(\"Invalid symbol sequence\") else r = ")
                            print("if (!r) return checkSource(state) else r = ")
                        }
                        s.generateCode(0, "$currentRuleName$i", out)
                    } else {
                        s.generateCode(indentLevel, "$currentRuleName$i", out)
                    }
                }
            }
        }
    }

    operator fun Symbol.rangeTo(other: Symbol): Symbol {
        return if (this is Sequence) {
            this.apply {
                symbols.add(other)
            }
        } else {
            Sequence().apply {
                symbols.add(this@rangeTo)
                symbols.add(other)
            }
        }
    }


    private class ZeroOrOne(val symbol: Symbol) : NonTerminal() {
        override fun toString(): String {
            return "{$symbol}?"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("if (r) {")
                indent {
                    println("state = save()")
                    print("r = ")
                }
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}ZoO", out)
                } else {
                    println("r${currentRuleName}ZoO(rn)", 0)
                    doLater {
                        func("r${currentRuleName}ZoO", body = symbol::generateCode)
                    }
                }
                println("} else return checkSource(state)")
                println("r = true")
            }
        }
    }

    val Symbol.zeroOrOne: Symbol
        get() = ZeroOrOne(this)


    private class ZeroOrMore(val symbol: Symbol) : NonTerminal() {
        override fun toString(): String {
            return "{$symbol}*"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("if (r) do {")
                indent {
                    println("state = save()")
                    print("r = ")
                }
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}ZoM", out)
                } else {
                    println("r${currentRuleName}ZoM(rn)", 0)
                    doLater {
                        func("r${currentRuleName}ZoM", body = symbol::generateCode)
                    }
                }
                println("} while (r) else return checkSource(state)")
                println("r = true")
            }
        }
    }

    val Symbol.zeroOrMore: Symbol
        get() = ZeroOrMore(this)


    private class OneOrMore(val symbol: Symbol) : NonTerminal() {
        override fun toString(): String {
            return "{$symbol}+"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("if (!r) return checkSource(state)")
                println("state = save()")
                print("r = ")
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}OoM", out)
                } else {
                    println("r${currentRuleName}OoM(rn)", 0)
                    doLater {
                        func("r${currentRuleName}OoM", body = symbol::generateCode)
                    }
                }
                println("if (r) do {")
                indent {
                    println("state = save()")
                    print("r = ")
                }
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}OoM", out)
                } else {
                    println("r${currentRuleName}OoM(rn)", 0)
                }
                println("} while (r) else return checkSource(state)")
                println("r = true")
            }
        }
    }

    val Symbol.oneOrMore: Symbol
        get() = OneOrMore(this)


    object N {
        sealed class CountNumber {
            object NCount : CountNumber() {
                override fun toString() = "n"
            }
            object ZeroCount : CountNumber() {
                override fun toString() = "0"
            }
            object OneCount : CountNumber() {
                override fun toString() = "1"
            }
        }
        data class Count(
            val first: CountNumber,
            val last: CountNumber
        )

        val n = CountNumber.NCount
        val zero = CountNumber.ZeroCount
        val one = CountNumber.OneCount

        operator fun CountNumber.rangeTo(end: CountNumber): Count {
            return Count(this, end)
        }

        operator fun Int.rangeTo(end: CountNumber): Count {
            return when (this) {
                0 -> zero..end
                1 -> one..end
                else -> throw RuntimeException("invalid range ($this .. $end)")
            }
        }

        operator fun get(symbol: Symbol): (N.() -> Count) -> Symbol {
            return { count ->
                val c = count(this)
                getSymbol(c, symbol)
            }
        }

        private fun getSymbol(c: Count, symbol: Symbol): Symbol {
            if (c.first === zero) {
                if (c.last === n) {
                    return ZeroOrMore(symbol)
                } else if (c.last === one) {
                    return ZeroOrOne(symbol)
                }
            } else if (c.first === one) {
                if (c.last === n) {
                    return OneOrMore(symbol)
                }
            }
            throw RuntimeException("invalid N range: $c")
        }
    }


    private open class RuleLink internal constructor(val rule: String, val callPrefix: String = "r") : Symbol {
        override val isTerminal = true

        override fun toString(): String {
            return "[$callPrefix$rule]"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("$callPrefix$rule(rn)")
            }
        }
    }

    fun R(rule: () -> String): Symbol {
        return Sequence().apply {
            symbols.add(RuleLink(rule()))
        }
    }


    private val rules = mutableMapOf<String, Symbol>()
    fun getAll(): Map<String, Symbol> {
        return rules.toMap()
    }

    fun rule(name: String, symbol: () -> Symbol) {
        rules[name] = symbol()
    }


    protected class Fragment(
        private val symbol: Symbol
    ) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): Symbol {
            return symbol
        }
    }
    private class SymbolHolder(symbol: () -> Symbol) : NonTerminal() {
        private val symbol by lazy(symbol::invoke)
        override fun toString(): String {
            return symbol.toString()
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            symbol.generateCode(indentLevel, currentRuleName, out)
        }
    }
    protected fun fragment(symbol: () -> Symbol) : Fragment {
        return Fragment(SymbolHolder(symbol))
    }


    /*private class NotSymbol(val symbol: Symbol) : Symbol {
        override val isTerminal = false

        override fun toString(): String {
            return "!$symbol"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.apply {
                println("if (r) r = rNot$currentRuleName() else throw RuntimeException()", indentLevel)
                doLater {
                    func("rNot$currentRuleName") { il, cn, w ->
                        w.apply {
                            println("try {", il)
                            symbol.generateCode(il + 1, cn, w)
                            println("r = false", il + 1)
                            println("} catch (err: Throwable) { r = true; skip(1) }", il)
                        }
                    }
                }
            }
        }
    }
    operator fun Symbol.not(): Symbol {
        return NotSymbol(this)
    }*/

    private class AnyChar : RuleLink("anyChar", "")
    val any: Symbol = AnyChar()

    private class WhiteSpace : Symbol {
        override val isTerminal = false
        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.println("ws()", indentLevel)
        }
        override fun toString(): String {
            return "[ws]"
        }
    }
    val ws: Symbol = WhiteSpace()

}