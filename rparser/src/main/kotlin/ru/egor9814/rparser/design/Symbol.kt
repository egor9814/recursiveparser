package ru.egor9814.rparser.design

import ru.egor9814.rparser.builder.SourceWriter

interface Symbol {
    val isTerminal: Boolean

    fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter)

}
