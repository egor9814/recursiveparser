package ru.egor9814.rparser.builder

import ru.egor9814.rparser.design.LangConfiguration
import java.io.File
import java.io.IOException
import java.io.PrintStream
import kotlin.math.roundToInt

class ParserBuilder(val conf: LangConfiguration) {

    fun build(outDir: File) {
        val logln = { msg: String ->
            println(msg)
        }
        val log = { msg: String ->
            print(msg)
        }

        logln("getting output source dir")
        val out = conf.packageName.let {
            when {
                it.isEmpty() -> outDir
                it.contains('.') -> File(outDir, it.split('.').joinToString(separator = "/"))
                else -> File(outDir, it)
            }
        }
        if (!out.exists()) {
            if (!out.mkdirs())
                throw IOException("cannot create output source dir")
        }
        logln("output source dir: ${out.absolutePath}")

        /*val file = File(out, "test.txt")
        if (!file.exists()) {
            if (!file.createNewFile())
                throw IOException("cannot create output source file")
        }
        file.outputStream().use {
            PrintStream(it).apply {
                conf.rules.getAll().forEach(this::println)
                println("\nmain = ${conf.mainRule}")
            }
        }*/

        logln("getting output parser file")
        val file = File(out, "${conf.langName}Parser.kt")
        if (!file.exists()) {
            if (!file.createNewFile())
                throw IOException("cannot create output source file")
        }
        logln("output parser file: ${file.absolutePath}")
        file.outputStream().use {
            PrintStream(it).apply {
                logln("generating parser...")
                println("/* Auto-Generated Recursive Parser by " +
                        "${conf.author.let { name -> if (name.size == 1) name[0] else name.joinToString() }} */")

                if (conf.packageName.isNotEmpty()) {
                    println("\npackage ${conf.packageName}")
                }

                println("\nimport ru.egor9814.rparser.lib.*")

                println("\nclass ${file.nameWithoutExtension}(sourceCode: SourceCode) : ParserBase(sourceCode) {")

                logln("  generating rules...")
                SourceWriter(this).apply {
                    var pct: Double
                    conf.rules.getAll().apply {
                        val step = 100.0 / (size + 2)
                        pct = step
                        forEach { (name, s) ->
                            log("    generating rule '$name'...")
                            val start = System.currentTimeMillis()
                            //func(name) { indent, currentName, writer -> s.generateCode(indent, currentName, writer) }
                            func("r$name", synthetic = false, body = s::generateCode)

                            val elapses = System.currentTimeMillis() - start
                            logln(" (${elapses}ms, ${pct.roundToInt()}%)")
                            pct += step
                        }
                    }
                    log("    generating sub rules...")
                    var start = System.currentTimeMillis()
                    doLater()
                    var elapses = System.currentTimeMillis() - start
                    logln(" (${elapses}ms, ${pct.roundToInt()}%)")

                    log("    generating main rule call...")
                    start = System.currentTimeMillis()
                    println("override fun main(result: Node): Boolean {", 1)
                    println("return r${conf.mainRule}(result)", 2)
                    println("}", 1)
                    elapses = System.currentTimeMillis() - start
                    logln(" (${elapses}ms, 100%)")
                }
                logln("  generating rules done!")

                println("}")
                logln("generating parser done!")
            }
        }

        logln("done!")
    }

}
