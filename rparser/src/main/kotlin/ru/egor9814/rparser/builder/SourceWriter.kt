package ru.egor9814.rparser.builder

import java.io.PrintStream

class SourceWriter(private val out: PrintStream) {

    var currentIndentLevel = 0

    private fun printIndent(indentLevel: Int) = apply {
        for (i in 1..indentLevel) {
            out.print("    ")
        }
    }

    fun print(value: String, indentLevel: Int = currentIndentLevel) = apply {
        printIndent(indentLevel)
        out.print(value)
    }

    fun println(value: String = "", indentLevel: Int = currentIndentLevel) = apply {
        printIndent(indentLevel)
        out.println(value)
    }


    fun indent(block: SourceWriter.() -> Unit) {
        val old = currentIndentLevel++
        block(this)
        currentIndentLevel = old
    }

    fun withIndent(indentLevel: Int, block: SourceWriter.() -> Unit) {
        val old = currentIndentLevel
        currentIndentLevel = indentLevel
        block(this)
        currentIndentLevel = old
    }


    private val laters = mutableListOf<SourceWriter.() -> Unit>()
    fun doLater(block: SourceWriter.() -> Unit) {
        laters.add(block)
    }
    fun doLater() {
        while (laters.isNotEmpty()) {
            val l = laters.toList()
            laters.clear()
            l.forEach { it() }
        }
    }


    fun func(
            name: String,
            access: String = "private",
            synthetic: Boolean = true,
            body: (Int, String, SourceWriter) -> Unit
    ) = apply {
        val acc = if (access.isEmpty()) "" else "$access "
        val nodeName = name.substring(1).capitalize()
        if (!synthetic) {
            println("class ${nodeName}Node : Node(\"$nodeName\")", 1)
        }
        println("${acc}fun $name(parentNode: Node?): Boolean {", 1)
        if (!synthetic) {
            println("ws()", 2)
            println("val rn = ${nodeName}Node()", 2)
        } else {
            println("val rn = parentNode", 2)
        }
        println("var r = true", 2)
        println("var state: Int? = null", 2)
        body(2, name, this)
        if (!synthetic) {
            println("if (r && parentNode is Node) parentNode.addChild(rn)", 2)
        }
        println("if (!r) checkSource(state)", 2)
        println("return r", 2)
        println("}", 1)
    }
}